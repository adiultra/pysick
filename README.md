# PySick
>So Sick of Python

This is my python code repo, I include in this repo some projects that I have thought of, or ideas I found tumbling over.

I do present some of the scripts info in this readme, but it is definetly not thorough. So please Don't blame me if you discover a feature, or a bug (Report that in issues);

## 16-bit-clock

This program is a command-line utility to display a 16-bit clock inspired by : https://github.com/lucasdnd/16-bit-clock/.

It's extremly similar, but also adds some punch by also outputting the current bits(fancy time units :smile:) as a list.

Don't forget to check my [blag](http://adilnx.tk).
